﻿using System;
using StandartLibrary;

namespace Task1_Core
{
    static class Program
    {
        static void Main(string[] args)
        {
            #region without library

            Console.Write("Please enter user name - ");
            var userName = Console.ReadLine();
            Console.WriteLine($"Hello, {userName}");

            #endregion

            Console.WriteLine(new string('=', 30));

            #region with library

            var currentTime = DateTime.Now;
            Console.WriteLine($"{currentTime} {userName.Concatanate()}");

            #endregion

            Console.ReadKey();
        }
    }
}
