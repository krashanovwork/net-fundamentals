﻿using System;
using System.Windows;
using StandartLibrary;

namespace Task1_WPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            #region without library

            string userName = textboxUserName.Text.ToString();
            MessageBox.Show($"Hello, {userName}");

            #endregion

            #region with library

            var currentTime = DateTime.Now;
            MessageBox.Show($"{currentTime} {userName.Concatanate()}");

            #endregion
        }
    }
}
